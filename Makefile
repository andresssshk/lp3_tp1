#Directorios raíces
SRC := ./tp-listing/src
BIN := ./tp-listing/bin

#Capítulos
CAP1 := capitulo_1
CAP2 := capitulo_2
CAP3 := capitulo_3
CAP4 := capitulo_4
CAP5 := capitulo_5

## Lista de listings ejecutables
LISTINGS := listing-1.2 listing-2.1 listing-2.2 listing-2.3 listing-2.4 listing-2.5 listing-2.6 listing-2.8 listing-2.9 listing-3.1 listing-3.2 listing-3.3 listing-3.4 listing-3.5 listing-3.6 listing-3.7 listing-4.1 listing-4.2 listing-4.3 listing-4.4 listing-4.5 listing-4.6 listing-4.7 listing-4.8 listing-4.9 listing-4.10 listing-4.11 listing-4.12 listing-4.13 listing-4.14 listing-4.15 listing-5.1 listing-5.2 listing-5.3 listing-5.4 listing-5.5 listing-5.6 listing-5.7 listing-5.8 listing-5.9 listing-5.10 listing-5.11 listing-5.12


## Capítulo 1

#Listing 1.1 al 1.3
#main.o
listing-1.1: PROGRAM = reciprocal
listing-1.1: NUMBER = 1.1
listing-1.1: $(SRC)/$(CAP1)/1.1/main.c $(SRC)/$(CAP1)/1.3/reciprocal.hpp
	@mkdir -p $(BIN)/$(CAP1)/$(NUMBER)/
	gcc $(CFLAGS) -c -o $(BIN)/$(CAP1)/$(NUMBER)/main.o -c $(SRC)/$(CAP1)/$(NUMBER)/main.c

#reciprocal.o y reciprocal
listing-1.2: PROGRAM = reciprocal
listing-1.2: NUMBER = 1.2
listing-1.2: $(SRC)/$(CAP1)/1.2/reciprocal.cpp $(SRC)/$(CAP1)/1.3/reciprocal.hpp listing-1.1
	@mkdir -p $(BIN)/$(CAP1)/$(NUMBER)/
	g++ $(CFLAGS) -c -o $(BIN)/$(CAP1)/$(NUMBER)/$(PROGRAM).o -c $(SRC)/$(CAP1)/$(NUMBER)/$(PROGRAM).cpp
	g++ $(CFLAGS) -o $(BIN)/$(CAP1)/$(NUMBER)/$(PROGRAM) $(BIN)/$(CAP1)/1.1/main.o $(BIN)/$(CAP1)/$(NUMBER)/$(PROGRAM).o
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP1)/$(NUMBER)/$(PROGRAM)'
	@echo 

listing-1.3: listing-1.2

## Capítulo 2

#Listing 2.1
listing-2.1: PROGRAM = arglist
listing-2.1: NUMBER = 2.1
listing-2.1:
	@mkdir -p $(BIN)/$(CAP2)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP2)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP2)/$(NUMBER)/$(PROGRAM).c
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP2)/$(NUMBER)/$(PROGRAM)'
	@echo 

#Listing 2.2
listing-2.2: PROGRAM = getopt_long
listing-2.2: NUMBER = 2.2
listing-2.2:
	@mkdir -p $(BIN)/$(CAP2)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP2)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP2)/$(NUMBER)/$(PROGRAM).c
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP2)/$(NUMBER)/$(PROGRAM)'
	@echo 

#Listing 2.3
listing-2.3: PROGRAM = print-env
listing-2.3: NUMBER = 2.3
listing-2.3:
	@mkdir -p $(BIN)/$(CAP2)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP2)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP2)/$(NUMBER)/$(PROGRAM).c
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP2)/$(NUMBER)/$(PROGRAM)'
	@echo 

#Listing 2.4
listing-2.4: PROGRAM = client
listing-2.4: NUMBER = 2.4
listing-2.4:
	@mkdir -p $(BIN)/$(CAP2)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP2)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP2)/$(NUMBER)/$(PROGRAM).c
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP2)/$(NUMBER)/$(PROGRAM)', ejecute \'export SERVER_NAME=nombre_server\' antes para "acceder" a otro server.
	@echo 

#Listing 2.5
listing-2.5: PROGRAM = temp_file
listing-2.5: NUMBER = 2.5
listing-2.5:
	@mkdir -p $(BIN)/$(CAP2)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP2)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP2)/$(NUMBER)/$(PROGRAM).c
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP2)/$(NUMBER)/$(PROGRAM)' y pase una palabra como ejemplo para utilizarla
	@echo 

#Listing 2.6
listing-2.6: PROGRAM = readfile
listing-2.6: NUMBER = 2.6
listing-2.6:
	@mkdir -p $(BIN)/$(CAP2)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP2)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP2)/$(NUMBER)/$(PROGRAM).c
	@echo Programa creado, ejecutelo con: "cat ./file > ./file && echo \"hola\" > ./file && $(BIN)/$(CAP2)/$(NUMBER)/$(PROGRAM) && rm file"
	@echo 

#Listing 2.7
listing-2.7: PROGRAM = test
listing-2.7: NUMBER = 2.7
listing-2.7:
	@mkdir -p $(BIN)/$(CAP2)/$(NUMBER)/
	gcc -c -o $(BIN)/$(CAP2)/$(NUMBER)/$(PROGRAM).o $(SRC)/$(CAP2)/$(NUMBER)/$(PROGRAM).c
	ar cr $(BIN)/$(CAP2)/$(NUMBER)/libtest.a $(BIN)/$(CAP2)/$(NUMBER)/$(PROGRAM).o
	gcc -c -o $(BIN)/$(CAP2)/$(NUMBER)/app.o $(SRC)/$(CAP2)/2.8/app.c
	gcc -o $(BIN)/$(CAP2)/$(NUMBER)/app $(BIN)/$(CAP2)/$(NUMBER)/app.o -L${HOME}/lp3_tp1/tp-listing/bin/$(CAP2)/$(NUMBER)/ -ltest
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP2)/$(NUMBER)/app; echo $$?'
	@echo 

#Listing 2.8
listing-2.8: listing-2.7

#Listing 2.9
listing-2.9: PROGRAM = tifftest
listing-2.9: NUMBER = 2.9
listing-2.9:
	@mkdir -p $(BIN)/$(CAP2)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP2)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP2)/$(NUMBER)/$(PROGRAM).c -ltiff
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP2)/$(NUMBER)/$(PROGRAM)'
	@echo 

## Capítulo 3

#Listing 3.1

#Listing 3.1
listing-3.1: PROGRAM = print-pid
listing-3.1: NUMBER = 3.1
listing-3.1:
	@mkdir -p $(BIN)/$(CAP3)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP3)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP3)/$(NUMBER)/$(PROGRAM).c
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP3)/$(NUMBER)/$(PROGRAM)'
	@echo 

#Listing 3.2
listing-3.2: PROGRAM = system
listing-3.2: NUMBER = 3.2
listing-3.2:
	@mkdir -p $(BIN)/$(CAP3)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP3)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP3)/$(NUMBER)/$(PROGRAM).c
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP3)/$(NUMBER)/$(PROGRAM)'
	@echo 

#Listing 3.3
listing-3.3: PROGRAM = fork
listing-3.3: NUMBER = 3.3
listing-3.3:
	@mkdir -p $(BIN)/$(CAP3)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP3)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP3)/$(NUMBER)/$(PROGRAM).c
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP3)/$(NUMBER)/$(PROGRAM)'
	@echo 

#Listing 3.4
listing-3.4: PROGRAM = fork-exec
listing-3.4: NUMBER = 3.4
listing-3.4:
	@mkdir -p $(BIN)/$(CAP3)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP3)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP3)/$(NUMBER)/$(PROGRAM).c
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP3)/$(NUMBER)/$(PROGRAM)'
	@echo 

#Listing 3.5
listing-3.5: PROGRAM = sigusr1
listing-3.5: NUMBER = 3.5
listing-3.5:
	@mkdir -p $(BIN)/$(CAP3)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP3)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP3)/$(NUMBER)/$(PROGRAM).c
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP3)/$(NUMBER)/$(PROGRAM)'
	@echo 

#Listing 3.6
listing-3.6: PROGRAM = zombie
listing-3.6: NUMBER = 3.6
listing-3.6:
	@mkdir -p $(BIN)/$(CAP3)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP3)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP3)/$(NUMBER)/$(PROGRAM).c
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP3)/$(NUMBER)/$(PROGRAM)'
	@echo 

#Listing 3.7
listing-3.7: PROGRAM = sigchld
listing-3.7: NUMBER = 3.7
listing-3.7:
	@mkdir -p $(BIN)/$(CAP3)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP3)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP3)/$(NUMBER)/$(PROGRAM).c
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP3)/$(NUMBER)/$(PROGRAM)'
	@echo 

## Capítulo 4

#Listing 4.1
listing-4.1: PROGRAM = thread-create
listing-4.1: NUMBER = 4.1
listing-4.1:
	@mkdir -p $(BIN)/$(CAP4)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP4)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP4)/$(NUMBER)/$(PROGRAM).c -lpthread
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP4)/$(NUMBER)/$(PROGRAM)'
	@echo 

#Listing 4.2
listing-4.2: PROGRAM = thread-create2
listing-4.2: NUMBER = 4.2
listing-4.2:
	@mkdir -p $(BIN)/$(CAP4)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP4)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP4)/$(NUMBER)/$(PROGRAM).c -lpthread
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP4)/$(NUMBER)/$(PROGRAM)'
	@echo 

#Listing 4.3
listing-4.3: PROGRAM = thread-create3
listing-4.3: NUMBER = 4.3
listing-4.3:
	@mkdir -p $(BIN)/$(CAP4)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP4)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP4)/$(NUMBER)/$(PROGRAM).c -lpthread
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP4)/$(NUMBER)/$(PROGRAM)'
	@echo 

#Listing 4.4
listing-4.4: PROGRAM = primes
listing-4.4: NUMBER = 4.4
listing-4.4:
	@mkdir -p $(BIN)/$(CAP4)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP4)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP4)/$(NUMBER)/$(PROGRAM).c -lpthread
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP4)/$(NUMBER)/$(PROGRAM)'
	@echo 

#Listing 4.5
listing-4.5: PROGRAM = detached
listing-4.5: NUMBER = 4.5
listing-4.5:
	@mkdir -p $(BIN)/$(CAP4)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP4)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP4)/$(NUMBER)/$(PROGRAM).c -lpthread
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP4)/$(NUMBER)/$(PROGRAM)'
	@echo 

#Listing 4.6
listing-4.6: PROGRAM = critical-section
listing-4.6: NUMBER = 4.6
listing-4.6:
	@mkdir -p $(BIN)/$(CAP4)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP4)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP4)/$(NUMBER)/$(PROGRAM).c -lpthread
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP4)/$(NUMBER)/$(PROGRAM)'
	@echo 

#Listing 4.7
listing-4.7: PROGRAM = tsd
listing-4.7: NUMBER = 4.7
listing-4.7:
	@mkdir -p $(BIN)/$(CAP4)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP4)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP4)/$(NUMBER)/$(PROGRAM).c -lpthread
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP4)/$(NUMBER)/$(PROGRAM)'
	@echo 

#Listing 4.8
listing-4.8: PROGRAM = cleanup
listing-4.8: NUMBER = 4.8
listing-4.8:
	@mkdir -p $(BIN)/$(CAP4)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP4)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP4)/$(NUMBER)/$(PROGRAM).c -lpthread
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP4)/$(NUMBER)/$(PROGRAM)'
	@echo 

#Listing 4.9
listing-4.9: PROGRAM = cxx-exit
listing-4.9: NUMBER = 4.9
listing-4.9:
	@mkdir -p $(BIN)/$(CAP4)/$(NUMBER)/
	g++ -o $(BIN)/$(CAP4)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP4)/$(NUMBER)/$(PROGRAM).cpp -lpthread
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP4)/$(NUMBER)/$(PROGRAM)'
	@echo 

#Listing 4.10
listing-4.10: PROGRAM = job-queue1
listing-4.10: NUMBER = 4.10
listing-4.10:
	@mkdir -p $(BIN)/$(CAP4)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP4)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP4)/$(NUMBER)/$(PROGRAM).c -lpthread
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP4)/$(NUMBER)/$(PROGRAM)'
	@echo 

#Listing 4.11
listing-4.11: PROGRAM = job-queue2
listing-4.11: NUMBER = 4.11
listing-4.11:
	@mkdir -p $(BIN)/$(CAP4)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP4)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP4)/$(NUMBER)/$(PROGRAM).c -lpthread
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP4)/$(NUMBER)/$(PROGRAM)'
	@echo 

#Listing 4.12
listing-4.12: PROGRAM = job-queue3
listing-4.12: NUMBER = 4.12
listing-4.12:
	@mkdir -p $(BIN)/$(CAP4)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP4)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP4)/$(NUMBER)/$(PROGRAM).c -lpthread
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP4)/$(NUMBER)/$(PROGRAM)'
	@echo 

#Listing 4.13
listing-4.13: PROGRAM = spin-condvar
listing-4.13: NUMBER = 4.13
listing-4.13:
	@mkdir -p $(BIN)/$(CAP4)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP4)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP4)/$(NUMBER)/$(PROGRAM).c -lpthread
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP4)/$(NUMBER)/$(PROGRAM)' y termine su ejecución con 'Ctrl+C'
	@echo 

#Listing 4.14
listing-4.14: PROGRAM = condvar
listing-4.14: NUMBER = 4.14
listing-4.14:
	@mkdir -p $(BIN)/$(CAP4)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP4)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP4)/$(NUMBER)/$(PROGRAM).c -lpthread
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP4)/$(NUMBER)/$(PROGRAM)'
	@echo 

#Listing 4.15
listing-4.15: PROGRAM = thread-pid
listing-4.15: NUMBER = 4.15
listing-4.15:
	@mkdir -p $(BIN)/$(CAP4)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP4)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP4)/$(NUMBER)/$(PROGRAM).c -lpthread
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP4)/$(NUMBER)/$(PROGRAM)' y terminelo con Ctrl + C
	@echo 

## Capítulo 5

#Listing 5.1
listing-5.1: PROGRAM = shm
listing-5.1: NUMBER = 5.1
listing-5.1:
	@mkdir -p $(BIN)/$(CAP5)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP5)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP5)/$(NUMBER)/$(PROGRAM).c
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP5)/$(NUMBER)/$(PROGRAM)'
	@echo 

#Listing 5.2
listing-5.2: PROGRAM = sem_all_deall
listing-5.2: NUMBER = 5.2
listing-5.2:
	@mkdir -p $(BIN)/$(CAP5)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP5)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP5)/$(NUMBER)/$(PROGRAM).c
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP5)/$(NUMBER)/$(PROGRAM)'
	@echo 

#Listing 5.3
listing-5.3: PROGRAM = sem_init
listing-5.3: NUMBER = 5.3
listing-5.3:
	@mkdir -p $(BIN)/$(CAP5)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP5)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP5)/$(NUMBER)/$(PROGRAM).c
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP5)/$(NUMBER)/$(PROGRAM)'
	@echo 

#Listing 5.4
listing-5.4: PROGRAM = sem_pv
listing-5.4: NUMBER = 5.4
listing-5.4:
	@mkdir -p $(BIN)/$(CAP5)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP5)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP5)/$(NUMBER)/$(PROGRAM).c
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP5)/$(NUMBER)/$(PROGRAM)'
	@echo 

#Listing 5.5
listing-5.5: PROGRAM = mmap-write
listing-5.5: NUMBER = 5.5
listing-5.5:
	@mkdir -p $(BIN)/$(CAP5)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP5)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP5)/$(NUMBER)/$(PROGRAM).c
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP5)/$(NUMBER)/$(PROGRAM)' nombre_del_archivo
	@echo 

#Listing 5.6
listing-5.6: PROGRAM = mmap-read
listing-5.6: NUMBER = 5.6
listing-5.6:
	@mkdir -p $(BIN)/$(CAP5)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP5)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP5)/$(NUMBER)/$(PROGRAM).c
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP5)/$(NUMBER)/$(PROGRAM)' nombre_del_archivo
	@echo 

#Listing 5.7
listing-5.7: PROGRAM = pipe
listing-5.7: NUMBER = 5.7
listing-5.7:
	@mkdir -p $(BIN)/$(CAP5)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP5)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP5)/$(NUMBER)/$(PROGRAM).c
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP5)/$(NUMBER)/$(PROGRAM)'
	@echo 

#Listing 5.8
listing-5.8: PROGRAM = dup2
listing-5.8: NUMBER = 5.8
listing-5.8:
	@mkdir -p $(BIN)/$(CAP5)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP5)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP5)/$(NUMBER)/$(PROGRAM).c
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP5)/$(NUMBER)/$(PROGRAM)'
	@echo 

#Listing 5.9
listing-5.9: PROGRAM = popen
listing-5.9: NUMBER = 5.9
listing-5.9:
	@mkdir -p $(BIN)/$(CAP5)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP5)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP5)/$(NUMBER)/$(PROGRAM).c
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP5)/$(NUMBER)/$(PROGRAM)'
	@echo 

#Listing 5.10
listing-5.10: PROGRAM = socket-server
listing-5.10: NUMBER = 5.10
listing-5.10:
	@mkdir -p $(BIN)/$(CAP5)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP5)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP5)/$(NUMBER)/$(PROGRAM).c
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP5)/$(NUMBER)/$(PROGRAM)'
	@echo 

#Listing 5.11
listing-5.11: PROGRAM = socket-client
listing-5.11: NUMBER = 5.11
listing-5.11:
	@mkdir -p $(BIN)/$(CAP5)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP5)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP5)/$(NUMBER)/$(PROGRAM).c
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP5)/$(NUMBER)/$(PROGRAM)' "mensaje" '(recuerde también ejecutar el servidor, listing-5.10)', puede salir '$(BIN)/$(CAP5)/$(NUMBER)/$(PROGRAM)' "quit"
	@echo 

#Listing 5.12
listing-5.12: PROGRAM = socket-inet
listing-5.12: NUMBER = 5.12
listing-5.12:
	@mkdir -p $(BIN)/$(CAP5)/$(NUMBER)/
	gcc -o $(BIN)/$(CAP5)/$(NUMBER)/$(PROGRAM) $(SRC)/$(CAP5)/$(NUMBER)/$(PROGRAM).c
	@echo Programa creado, ejecutelo con: '$(BIN)/$(CAP5)/$(NUMBER)/$(PROGRAM)'
	@echo 

all: $(LISTINGS)

clean:
	@rm -f -r tp-listing/bin/*