#include <stdio.h>
#include <unistd.h>
int main()
{
    printf("El PID de este proceso es:  %d\n", (int)getpid());
    printf("PID del Padre es: %d\n", (int)getppid());
    return 0;
}