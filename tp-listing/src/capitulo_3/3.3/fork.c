#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
int main()
{
    // creando variable de un proceso hijo
    pid_t child_pid;

    printf("El ID del proceso principal es: %d\n\n", (int)getpid());

    /** Duplica este proceso(el padre digamos)
     * 
     * Con duplicar se refuere que copia todo el lo que contiene en la memoria ram el proceso del padre
     * y lo pega en uno nuevo para el hijo, lo mismo pasa con el descriptor.
     * 
     * obs.: tambien copia el programcounter(el registro que guarda donde apuntar el puntero que
     * apunta que linea de ram esta ejecutando en ese instante.
     * 
     * lo que no duplica seria el PID
     * el hijo tendra uno nuevo
     * 
     * tambien duplica el descriptor 
    */
    child_pid = fork();

    if (child_pid != 0)
    {
        printf("Este es el proceso es pariente de ID: %d\n\n", (int)getpid());
        printf("el ID del proceso hijo es: %d\n\n", (int)child_pid);
    }
    else
        printf("this is the child process, with id %d\n\n", (int)getpid());
    return 0;
}

//comentario random
