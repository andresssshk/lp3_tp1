//Program Fragment Demonstrating a Thread
//Cleanup Handler

#include <malloc.h>
#include <pthread.h>
#include <stdio.h>
#include <string.h>
/* Allocate a temporary buffer.*/

void* allocate_buffer (size_t size)
{
	return malloc (size);
}

/* Deallocate a temporary buffer.*/

void deallocate_buffer (void* buffer)
{
	fclose((FILE*)buffer);
	free (buffer);
}

void do_some_work ()
{
 
	/* Allocate a temporary buffer.*/
	char filename[] = "notes.txt";
	char cadena[] = "Mostrando el uso de fputs en un fichero.\n";
	void* fp = allocate_buffer (8);
	fp = (FILE*)fopen ( filename, "w+" );
	/*Abrimos un archivo con este nombre*/

	/* Register a cleanup handler for this buffer, to deallocate it in
	case the thread exits or is cancelled. */
	pthread_cleanup_push (deallocate_buffer, fp);
	
	/* Do some work here that might call pthread_exit or might be
	cancelled... */

 	if (fp==NULL) {
 		fputs ("File error",stderr); 
 		free(fp);
 		pthread_exit(0);
 	}else{
 		fputs( cadena, fp );
 	}

	/* Unregister the cleanup handler. Because we pass a nonzero value,
	this actually performs the cleanup by calling
	deallocate_buffer. */
	
	pthread_cleanup_pop (1);
}

int main(){
	
	do_some_work();
	return 0;
}
