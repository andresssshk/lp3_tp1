#include <malloc.h>
#include <stdio.h>
#include <pthread.h>


struct job {
    /* Link field for linked list.*/
    struct job* next;

    /* Other fields describing work to be done...*/
};

/* A linked list of pending jobs.*/
struct job* job_queue;

/* Process queued jobs until the queue is empty.*/

void process_job(struct job* job){
    printf("trabajo %p, tomado de la cola\n", job);

}


void* thread_function (void* arg){
    while (job_queue != NULL) {
        /* Get the next available job. */
        struct job* next_job = job_queue;

        /* Remove this job from the list.*/
        job_queue = job_queue->next;
        /* Carry out the work. */
        process_job (next_job);
        /* Clean up. */
        free (next_job);
    }
    return NULL;
}

int main(){
    job_queue=(struct job *)malloc(sizeof(struct  job));
    job_queue->next=NULL;

    pthread_t thread1_id;
    pthread_create (&thread1_id, NULL, &thread_function, NULL);

    pthread_join(thread1_id, NULL);



    return 0;
}