#include <pthread.h>
#include <stdio.h>
//#define NUM_THREADS 3

/* Prints x’s to stderr.
The parameter is unused.
Does not return.
*/
void* print_xs(void *unused)
{
	while (1)
	{
		fputc('x', stderr);
	}
	return NULL;
}

/* The main program.*/
int main()
{

	int id_Hilo;

	/**
	 * Crea el descriptor de Hilo
	*/
	pthread_t thread_id;

	/* Create a new thread. The new thread will run the print_xs
	function. */

	// al crear el hilo retorna un INT que es el id del Hilo
	id_Hilo = pthread_create(&thread_id, NULL, &print_xs, NULL);
	printf("id Hilo =%d", id_Hilo);

	/* Print o’s continuously to stderr. */
	int n = 0;
	while (n < 1200)
	{
		fputc('o', stderr);
		n++;
	}
	return 0;
}


/*
Para compilar agregar un -lpthread al final
ejemplo

gcc thread-create.c -o main -lpthread
*/