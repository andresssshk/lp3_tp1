//Protect a Bank Transaction with a Critical Section
#include <pthread.h>
#include <stdio.h>
#include <string.h>

/* An array of balances in accounts, indexed by account number.*/
int account_number = 20;
float account_balances[20];

/* Transfer DOLLARS from account FROM_ACCT to account TO_ACCT. Return
0 if the transaction succeeded, or 1 if the balance FROM_ACCT is
too small. */

typedef struct transaccion{
	int from;
	int to;
	float monto;
}Transfer;


int process_transaction (int from_acct, int to_acct, float dollars)
{
	int old_cancel_state;

	/* Check the balance in FROM_ACCT. */
	if (account_balances[from_acct] < dollars)
		return 1;

	/* Begin critical section. */
	pthread_setcancelstate (PTHREAD_CANCEL_DISABLE, &old_cancel_state);
	/* Move the money. */
	account_balances[to_acct] += dollars;
	account_balances[from_acct] -= dollars;
	/* End critical section. */
	pthread_setcancelstate (old_cancel_state, NULL);
	
	return 0;
}

void* operacion(void* parameter){

	Transfer* dato = (Transfer*) parameter;
	printf("Transferencia de %d a %d por un monto de %f\n", dato->from, dato->to, dato->monto);
	int answer;
	answer = process_transaction(dato->from, dato->to, dato->monto);
	if(answer)
		printf("Error en la transferencia\n");
	else
		printf("Datos actualizados\n%d->%f\n%d->%f\n",dato->from, account_balances[dato->from], dato->to, account_balances[dato->to]);
	return NULL;
}

void llenadoDeElementos(){

	int i;
	for( i=0; i<account_number; i++ ){
		account_balances[i] = 50.00;
	}

}

void ImprimirMatriz(){
	printf("Elementos de account_balances\n");
	int i;
	for( i=0; i<account_number; i++ )
		printf("%d->%f\n", i, account_balances[i]);
	printf("\n");
}

int main(){
	float account_balances[account_number];
	llenadoDeElementos();
	ImprimirMatriz();
	/*Datos para la transferencia*/
	Transfer nuevo;
	nuevo.from = 2; 
	nuevo.to = 5;
	nuevo.monto = 30.00;

	pthread_t thread;
	pthread_create(&thread, NULL, &operacion, &nuevo);

	pthread_join(thread, NULL);
	return 0;
}