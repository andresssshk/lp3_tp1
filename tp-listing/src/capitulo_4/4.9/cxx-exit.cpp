#include <stdio.h>
#include <pthread.h>

class ThreadExitException{
    public:
        /* create an exception-signaling thread exit with RETURN_VALUE*/
        ThreadExitException(void* return_value) : thread_return_value_(return_value)

    {
}
    /*actuallu exit the thread, using the return value provided in the constructor*/
    void* DoThreadExit(){
        pthread_exit(thread_return_value_);
    }

    private:
        /*the return value that will be used when exiting the thread*/
        void* thread_return_value_;
};

void do_some_work(){
    while(1){
        /*do some useful things here*/
        if(/* should_exit_thread_immediately () */ 1)
            throw ThreadExitException(/*thread's return value= */ NULL);
    }
}

void* thread_function(void*){
    try{
        do_some_work();
    }catch (ThreadExitException ex){
        /*some function indicated that we should exit the thread*/
        ex.DoThreadExit();
    }
    return NULL;
}

int main(int argc, char const *argv[])
{
    int id_Hilo;

	/**
	 * Crea el descriptor de Hilo
	*/
	pthread_t thread_id;

	// al crear el hilo retorna un INT que es el id del Hilo
	id_Hilo = pthread_create(&thread_id, NULL, &thread_function, NULL);

    return pthread_join(thread_id, NULL);
}
