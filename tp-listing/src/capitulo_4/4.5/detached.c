//Skeleton Program That Creates a Detached Thread
#include <pthread.h>
#include <stdio.h>


void* factorial_function(void* arg)
{
	int n = *((int*) arg); /* variable local */
	int i;
	int producto = 1;
	/* Calcula el producto n*(n-1)*(n-2)*...*2*1 */
	for (i = n; i > 1; --i) 
		producto *= i;

	/* Imprime el resultado de la funcion */
	printf("El factorial de %d es %d\n", n, producto);

	return NULL;
} 

int main ()
{

	int n = 8;

	pthread_attr_t attr;
	pthread_t thread;
	pthread_attr_init (&attr);
	pthread_attr_setdetachstate (&attr, PTHREAD_CREATE_DETACHED);
	pthread_create (&thread, &attr, &factorial_function, &n);
	pthread_attr_destroy (&attr);

	/* Do work here...*/

	int j= 0;
	for(int i=0; i<200000; i++){
		j++;
	}

	/* No need to join the second thread. */

	return 0;
}
