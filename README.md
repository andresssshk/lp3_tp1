# lp3_tp1

# Lenguajes de Programación III –Trabajo Práctico Nro.1
Conforme al contenido programático de la cátedra y al contenido de libro “Advanced Linux Programing”, el Trabajo Práctico Nro. 1 se define como:“Implementar enlenguajeC/C++ los ejemplosprogramáticos indicados en los listings de los capítulos 1 a 5 del libro”. **Para ello:**
* Los alumnos estarán habilitados a formar grupos de hasta 4 integrantes.
* El TP deberá ser entregado en la forma de un Proyecto de Programación en  C/C++versionado con GIT en las plataformas Github o Gitlab.
* El desarrollo deberá estarorganizado conforme a la figura de abajo:
    * Se  deberá  identificarun  directorio  principal, por  ejemplo  “TP-LISTINGS”,  el  cual contendrá los subdirectorios “`src`” y “`bin`” 
    * El subdirectorio “`src`”, contendrá a subdirectorios nombrados conforme a los capítulos del libro y dentro de los mismos se encontrarán los archivos fuentes que implementan los listings de cada capítulo.
    * De igual forma, el subdirectorio “`bin`” contendrá subdirectorios nombrados de acuerdo a  los  capítulos  del  libro  y  estos  a  su  vez  contendrán  los  programas  compilados correspondientes a los listings.
    ![](tp_listings.png)
* Adicionalmente,    el    Proyecto    de    Desarrollo    deberá    estar    soportado    con    la    utilidad **GNU  Make**,  tal  que se automatice  la  compilación  de  los diferentes  programas  y  como mínimo puedan ser soportadaslassiguientesdirectivas:
    * `make all`, que compila todos los programas desarrollados.
    * `make clean`, que elimina todas la compilaciones existentes.
    * `make listing-X.Y`, que permite compilar el ejemplo X.Y del capítulo X
        * Ej. “`make listing-3.1`”, que compila el listing 3.1 (`print-pid.c`) del capítulo 3.

<mark>_**Observación importante**_</mark>: _Se deberá completar o complementar el código de aquellos listingsque así lo requieran,de forma tal a convertirlos en programas completos, compilablesy pasibles de ejecución._